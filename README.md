
#Instru��es para execu��o:

A fun��o "main" do projeto encontra-se no ficheiro BySide dentro do package byside localizado no diret�rio
src do projeto, sendo que basta arrancar o mesmo de forma a visualizar o trabalho realizado.

#Explica��o da solu��o:

Sendo que o tempo foi limitado (<24h), optei por realizar a solu��o mais simples, atendendo a
todos os requisitos pedidos no enunciado. N�o inclui uma interface para intera��o,no entanto,
para todas as funcionalidades coloquei um conjunto de testes que possibilitam a visualiza��o
das mesmas.
Estruturei o projeto em tr�s packages, controllers, models e servi�os onde tentei organizar o 
c�digo da melhor forma. Nos controllers incluo as funcionalidades propriamente ditas, nos models
as entidades do neg�cio e nos servi�os apenas incluo uma listagem em �rvore dos diret�rios
e ficheiros criados.