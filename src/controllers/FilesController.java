/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import models.File;
import models.Folder;
import models.Permission;

/**
 *
 * @author Joao
 */
public class FilesController {

    private static final String GENERIC_ERROR = "Erro";
    private static final String SUCCESS_MOVING = "Movido com sucesso";

    public File createFile(Folder locationFolder, String name) {
        //Should respect user creating
        Permission permission = new Permission(true, true, true);
        File f = new File(name, permission);
        if (locationFolder.addFile(f)) {
            return f;
        }
        return null;
    }

    public String moveFileByName(String fileName, Folder originFolder, Folder destinationFolder) {

        File f = originFolder.getFileByName(fileName);

        if (originFolder.removeFileByName(fileName)) {

            if (destinationFolder.addFile(f)) {
                return SUCCESS_MOVING;
            } else {
                return GENERIC_ERROR;
            }
        }
        return GENERIC_ERROR;
    }

    public String copyFileByName(String name, Folder origin, Folder destination) {
        File f = origin.getFileByName(name);
        if (destination.addFile(f)) {
            return SUCCESS_MOVING;
        } else {
            return GENERIC_ERROR;
        }
    }

}
