/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import models.File;
import models.Folder;
import models.Permission;

/**
 *
 * @author Joao
 */
public class FoldersController {

    private static final String GENERIC_ERROR = "Erro";
    private static final String SUCCESS_MOVING = "Movido com sucesso";

    public Folder createFolder(Folder locationFolder, String name) {
        //Should respect user creating
        Permission permission = new Permission(true, true, true);
        Folder f = new Folder(name, permission);
        if (locationFolder.addFolder(f)) {
            return f;
        }
        return null;
    }

    public String moveFolderByName(String folderName, Folder originFolder, Folder destinationFolder) {

        Folder f = originFolder.getFolderByName(folderName);

        if (originFolder.removeFolderByName(folderName)) {
            if (destinationFolder.addFolder(f)) {
                return SUCCESS_MOVING;
            } else {
                return GENERIC_ERROR;
            }
        }
        return GENERIC_ERROR;
    }

    public String copyFolderByName(String name, Folder origin, Folder destination) {
        Folder f = origin.getFolderByName(name);
        if (destination.addFolder(f)) {
            return SUCCESS_MOVING;
        } else {
            return GENERIC_ERROR;
        }
    }
}
