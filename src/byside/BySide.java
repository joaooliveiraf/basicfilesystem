/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package byside;

import controllers.FilesController;
import controllers.FoldersController;
import models.File;
import models.Folder;
import models.Permission;
import services.FilesystemBasicService;
/**
 *
 * @author Joao
 */
public class BySide {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //initialization
        Permission permissionExample = new Permission(true,true,true);         
        Folder root = new Folder("root",permissionExample);
        
        FilesystemBasicService displaySvc = new FilesystemBasicService();
        FilesController filesCtrl = new FilesController();
        FoldersController folderCtrl = new FoldersController();
        //initialization end
        
        
        //Creating folders test
        System.out.println("\n\nBefore creating etc and bin at root:\n");
        displaySvc.showDirectoriesTree(root);  
        Folder etc = folderCtrl.createFolder(root, "etc");
        Folder bin = folderCtrl.createFolder(root, "bin");   
        System.out.println("\n\nAfter creating etc and bin at root:\n");
        displaySvc.showDirectoriesTree(root);  
        //Creating folders test end
        
        
        
        //Creating file test
        System.out.println("\n\nBefore creating file1 and file2 at etc:\n");
        displaySvc.showDirectoriesTree(root);  
        File file1 = filesCtrl.createFile(etc, "file1");
        File file2 = filesCtrl.createFile(etc, "file2");           
        System.out.println("\n\nAfter creating file1 and file2 at etc:\n");
        displaySvc.showDirectoriesTree(root);
        //Creating file test end   
        
       
        
        //Moving file test
        System.out.println("\n\nBefore Moving file1 from etc to bin:\n");    
        displaySvc.showDirectoriesTree(root);       
        filesCtrl.moveFileByName(file1.getName(), etc, bin);      
        System.out.println("\n\nAfter Moving file1 from etc to bin:\n");      
        displaySvc.showDirectoriesTree(root);
        //Moving file test end
        
        
        //Moving folder test 
        System.out.println("\n\nBefore Moving bin from root to etc:\n");    
        displaySvc.showDirectoriesTree(root);       
        folderCtrl.moveFolderByName(bin.getName(), root, etc);      
        System.out.println("\n\nAfter Moving bin from root to etc:\n");      
        displaySvc.showDirectoriesTree(root);
        //Moving file test end
        
        //Copying file test end
        System.out.println("\n\nBefore copying file1 at bin from root to etc:\n");    
        displaySvc.showDirectoriesTree(root);       
        filesCtrl.copyFileByName(file1.getName(), bin, etc);      
        System.out.println("\n\nAfter copying file1 at bin from root to etc:\n");      
        displaySvc.showDirectoriesTree(root);
        //Copying file test end
        
        //Copying folder test
        System.out.println("\n\nBefore copying bin at etc to root:\n");    
        displaySvc.showDirectoriesTree(root);       
        folderCtrl.copyFolderByName(bin.getName(), etc, root);      
        System.out.println("\n\nAfter copying bin at etc to root:\n");      
        displaySvc.showDirectoriesTree(root);
        //Copying folder test end
        
        
        
        //all info 
        System.out.println("\n\nUsed objects details:\n");
        //show folder content
        System.out.println("Folders:\n"+root.toString()+"\n"+etc.toString() +"\n"+bin.toString());
        //show file content
        System.out.println("Files:\n" + file1.toString() +"\n"+file2.toString());
    }
    
}
