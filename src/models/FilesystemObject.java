/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Joao
 */
public abstract class FilesystemObject {
    private String name;
    private Permission permission;
    private Date creationDate;
    
    public FilesystemObject(String name, Permission permission) {
        this.name = name;
        this.permission = permission;
	this.creationDate = new Date();
        
    }

    public String getName() {
        return name;
    }

    public Permission getPermission() {
        return permission;
    }

    public Date getCreationDate() {
        return creationDate;
    }
    
    
    @Override
    public boolean equals(Object other) {   
       //file is the same if same name
       if (other instanceof File){
        if (((FilesystemObject)other).name.contentEquals(this.name)){
            return true;
        }
    }
    return false;
    }

    @Override
    public String toString() {
        return "{" + "name=" + name + ", permission=" + permission + ", creationDate=" + creationDate + '}';
    }
    
    
    
}
