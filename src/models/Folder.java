/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class Folder extends FilesystemObject {

    private ArrayList<Folder> folders;
    private ArrayList<File> files;

    public Folder(String name, Permission permission) {
        super(name, permission);
        folders = new ArrayList<>();
        files = new ArrayList<>();
    }

    public boolean addFile(File file) {
        if (file != null && !files.contains(file)) {
            return files.add(file);
        }
        return false;
    }

    public boolean addFolder(Folder folder) {
        if (folder != null && !folders.contains(folder)) {
            return folders.add(folder);
        }
        return false;
    }

    
    
    public boolean removeFileByName(String fileName) {
        return files.removeIf(file -> file.getName().contentEquals(fileName));
    }
    public File getFileByName(String fileName){
        return files.stream().filter(file-> file.getName().contentEquals(fileName)).findFirst().orElse(null);
    }
    
    
    
    public boolean hasAnyFolder(){
        return folders.size()>0;
    }
    
    
    
    public Folder getFolderByName(String folderName){
        return folders.stream().filter(folder-> folder.getName().contentEquals(folderName)).findFirst().orElse(null);
    }
    public boolean removeFolderByName(String folderName) {
        return folders.removeIf(file -> file.getName().contentEquals(folderName));
    }
    
    
    
    public ArrayList<Folder> getFolders(){
        return folders;
    }
    public ArrayList<File> getFiles(){
        return files;
    }

}
