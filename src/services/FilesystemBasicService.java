/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;
import models.File;
import models.Folder;

/**
 *
 * @author Joao
 */
public class FilesystemBasicService {

    public void showDirectoriesTree(Folder root) {

        if (root != null) {
            showDirectoriesTreeRec(root, 1);
        }

    }

    private void showDirectoriesTreeRec(Folder folder, int level) {
        
        System.out.println(indentFilesystemObjectName(folder.getName(), level));
        displayFiles(folder.getFiles(), level);
        
        if (!folder.hasAnyFolder()) {
            return;
        }
        level++;
        for (Folder f : folder.getFolders()) {
            showDirectoriesTreeRec(f, level);
        }
    }

    private void displayFiles(ArrayList<File> files, int level) {
        for (File f : files) {
            System.out.println("-"+indentFilesystemObjectName(f.getName(), level));
        }
    }

    private String indentFilesystemObjectName(String name, int indentation) {
        String indentatedStr = "";
        for (int i = 0; i < indentation; i++) {
            indentatedStr += "-";
        }
        return indentatedStr.concat(name);
    }
}
